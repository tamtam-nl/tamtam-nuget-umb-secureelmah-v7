﻿using System;
using System.Web;
using System.Xml;
using System.IO;
using System.Configuration;
using System.Data.SqlClient;
using Umbraco.Web;
using System.Linq;

namespace TamTam.NuGet.Umb.SecureElmah
{
    public class Module : IHttpModule
    {
        private static bool _hasAppStarted = false;
        private readonly static object _syncObject = new object();
        private static string _elmahFile = "elmah.axd";
        private static string _elmahConnectionString = string.Empty;

        public void Init(HttpApplication application)
        {
            lock (_syncObject)
            {
                if (!_hasAppStarted)
                {
                    var installed = ConfigurationManager.AppSettings["umbracoConfigurationStatus"];
                    if (!String.IsNullOrWhiteSpace(installed))
                    {
                        InitializeModule(application);
                    }
                    _hasAppStarted = true;
                }
            }

            //application.Error += new EventHandler(application_Error); // <-- ELMAH takes cares of it! http://code.google.com/p/elmah/
            application.PreRequestHandlerExecute += new EventHandler(application_PreRequestHandlerExecute);
        }

        private void InitializeModule(HttpApplication application)
        {
            //Get the elmah path from the web.config
            var configFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web.config");
            var xDoc = new XmlDocument();
            xDoc.Load(configFile);
            var elmahLocationNode = xDoc.SelectSingleNode("//handlers/add[translate(@name,'ELMAH','elmah')='elmah']");
            if (elmahLocationNode != null)
            {
                _elmahFile = elmahLocationNode.Attributes["path"].Value;
            }

            //Check if elmah is in the reserved paths (needed for Elmah to work together with SEO Checker)
            var myWebConfig = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~");
            if (!myWebConfig.AppSettings.Settings["umbracoReservedUrls"].Value.ToLower().Split(',').Any(u => u == "~/" + _elmahFile))
            {
                myWebConfig.AppSettings.Settings["umbracoReservedUrls"].Value += ",~/" + _elmahFile;
                myWebConfig.Save();
                return;
            }

            //If no valid sqlconnection exists: set the sqlconnection for elmah equal to the Umbraco one
            var elmahConnectionstringExists = false;
            var elmahConnectionstringWorks = false;
            var con = ConfigurationManager.ConnectionStrings["elmah-sqlserver"];
            if (con != null)
            {
                elmahConnectionstringExists = true;
                using (var cn = new SqlConnection(con.ConnectionString))
                {
                    try
                    {
                        cn.Open();
                        elmahConnectionstringWorks = true;
                    }
                    catch { }
                    finally
                    {
                        cn.Close();
                        cn.Dispose();
                    }
                }
            }

            if (!elmahConnectionstringExists || !elmahConnectionstringWorks)
            {
                var umbracoCon = ConfigurationManager.ConnectionStrings["umbracoDbDSN"].ConnectionString;
                if (!string.IsNullOrEmpty(umbracoCon))
                {
                    if (!elmahConnectionstringExists)
                    {
                        myWebConfig.ConnectionStrings.ConnectionStrings.Add(new ConnectionStringSettings("elmah-sqlserver", umbracoCon));
                        myWebConfig.Save();
                    }
                    if (!elmahConnectionstringWorks && myWebConfig.ConnectionStrings.ConnectionStrings["elmah-sqlserver"].ConnectionString != umbracoCon)
                    {
                        myWebConfig.ConnectionStrings.ConnectionStrings["elmah-sqlserver"].ConnectionString = umbracoCon;
                        myWebConfig.Save();
                    }
                }
                return;
            }


            using (var cn = new SqlConnection(con.ConnectionString))
            {
                try
                {
                    cn.Open();
                    var tables = cn.GetSchema("Tables");
                    if (tables.Select("TABLE_NAME='ELMAH_Error'").Length == 0)
                    {
                        var sqlFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "App_Readme", "Elmah.SqlServer.sql");
                        FileInfo file = new FileInfo(sqlFile);
                        string script = file.OpenText().ReadToEnd();
                        var scriptParts = script.Split(new string[] { "GO\r\n" }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (var part in scriptParts)
                        {
                            var sql = part.Trim();
                            if (!string.IsNullOrEmpty(sql))
                            {
                                using (var cmd = new SqlCommand(sql, cn))
                                {
                                    try
                                    {
                                        cmd.ExecuteNonQuery();
                                    }
                                    catch (Exception ex)
                                    {
                                        throw new System.ApplicationException(string.Format("Error running ELMAH Sql Script step:\r\n", sql), ex);
                                    }
                                }
                            }
                        }
                    }
                }
                finally
                {
                    cn.Close();
                    cn.Dispose();
                }

            }
        }

        void application_PreRequestHandlerExecute(object sender, EventArgs e)
        {
            var context = HttpContext.Current;
            // Get the path being requested
            string path = context.Request.Path.ToLower();

            // Check if it's elmah.axd
            if (!String.IsNullOrEmpty(path) && path.Contains("/" + _elmahFile))
            {
                // Verify that a Umbraco admin user is logged in
                var currentUmbracoUser = UmbracoContext.Current.UmbracoUser;

                bool isValid =
                (
                    currentUmbracoUser != null
                    && currentUmbracoUser.UserType != null
                    && !String.IsNullOrEmpty(currentUmbracoUser.UserType.Alias)
                );

                // If invalid, redirect to Umbraco LogIn Page
                if (!isValid)
                {
                    context.Response.Redirect("/umbraco#/login/false");
                }
            }
        }
        public void Dispose()
        {

        }

    }
}